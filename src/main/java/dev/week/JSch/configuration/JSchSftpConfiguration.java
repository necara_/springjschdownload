
package dev.week.JSch.configuration;

import com.jcraft.jsch.JSch;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JSchSftpConfiguration {

	@Bean
	public JSch JSch(){
		return new JSch();
	}
}
