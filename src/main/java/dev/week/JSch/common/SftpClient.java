package dev.week.JSch.common;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;

@Component
public class SftpClient {

	@Autowired
	private JSch jsch;

	public Session connectSession() {
		JSch.setConfig("StrictHostKeyChecking", "no");
		try {
			Session session = jsch.getSession("nemanja","localhost");
			session.setPassword("pass123");
			session.connect();
			return session;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	public void disconnectSession(Session session) {
		if (session != null) {
			session.disconnect();
		}
		JSch.setConfig("StrictHostKeyChecking", "yes");
	}

	public ChannelSftp connectChannel(Session session) {
		ChannelSftp channelSftp;
		try {
			channelSftp = (ChannelSftp) session.openChannel("sftp");
			channelSftp.connect();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException();
		}
		return channelSftp;
	}

	public void disconnectChannel(ChannelSftp channelSftp) {
		if (channelSftp != null) {
			channelSftp.disconnect();
		}
	}

	public void changeDirectory(ChannelSftp channel, String remotePath) {
		try {
			channel.cd(remotePath);
		} catch (Exception e) {
			System.out.println(e);
			throw new RuntimeException();
		}
	}

	public InputStream getFile(ChannelSftp channel, String fileName) {
		try {
			return channel.get(fileName);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException();
		}
	}
}
