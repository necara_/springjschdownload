
package dev.week.JSch.service.impl;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import dev.week.JSch.common.SftpClient;
import dev.week.JSch.service.JSchDownloadService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;


@Service
public class JSchDownloadServiceImpl implements JSchDownloadService {

	@Autowired
	SftpClient sftpClient;

	@Override
	public void download() {

		Session session = sftpClient.connectSession();
		ChannelSftp channel = null;

		try {
			channel = sftpClient.connectChannel(session);
			sftpClient.changeDirectory(channel, "/sftp/upload");
			System.out.println("Directory: " + channel.pwd());

			try (InputStream inputStream = sftpClient.getFile(channel, "a_large.txt")) {
				String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
				System.out.println(result);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				throw e;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sftpClient.disconnectChannel(channel);
			sftpClient.disconnectSession(session);
		}
	}
}
