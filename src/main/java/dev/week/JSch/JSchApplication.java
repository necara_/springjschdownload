package dev.week.JSch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JSchApplication {

	public static void main(String[] args) {
		SpringApplication.run(JSchApplication.class, args);
	}

}
