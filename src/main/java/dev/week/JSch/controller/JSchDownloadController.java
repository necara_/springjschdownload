package dev.week.JSch.controller;

import dev.week.JSch.service.impl.JSchDownloadServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JSchDownloadController {

	@Autowired
	private JSchDownloadServiceImpl jschDownloadService;

	@GetMapping("/download")
	public void downloadFileJSch() {
		jschDownloadService.download();
	}
}
